import { IconSparkles, IconTable } from '/src/icons'
import s from './Icon.module.css'

const getIcon = (iconTitle) => {
  switch (iconTitle) {
    case 'sparkles':
      return <IconSparkles />
    case 'table':
      return <IconTable />
  }
}

export const Icon = ({ variant }) => {
  return(
    <div className={s.root}>
      {getIcon(variant)}
    </div>
  )
}