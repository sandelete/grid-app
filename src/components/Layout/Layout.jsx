import { Outlet, Link, useLocation, useParams } from 'react-router-dom'
import { Icon } from '/src/ui'
import s from './Layout.module.css'
import clsx from 'clsx'

const HeaderLinks = ({ routes }) => {
  const { pathname } = useLocation()

  return (
    <div className={s.links}>
      {routes.map(({ icon, path, title }) => (
        <Link
          to={path}
          className={clsx(s.link, { [s.active]: pathname === path, [s.disabled]: !path })}
          key={title}
        >
          {icon && <Icon variant={icon} />}
          {title}
        </Link>
      ))}
    </div>
  )
}

export const Layout = ({ routes }) => {
  return (
    <div className={s.root}>
      <header className={s.header}>
        <HeaderLinks routes={routes} />
      </header>
      <main>
        <Outlet />
      </main>
    </div>
  )
}
