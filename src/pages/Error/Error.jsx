export const Error = () => {
  let error = useRouteError();
  console.error(error);
  return (
    <div style={{ padding: '64px', color: 'salmon' }}>
      <div style={{ fontSize: '32px' }}>{error.message}</div>
      <div style={{ fontSize: '14px', marginTop: '24px' }}>{error.stack}</div>
    </div>
  );
}