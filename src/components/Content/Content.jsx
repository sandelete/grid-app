import { motion } from "framer-motion"
import s from './Content.module.css'
import {Box} from '/src/ui'

export function ContentA() {
  return(
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ ease: "easeInOut", duration: 1 }}
      className={s.root}
      key="ContentA"
    >

      <Box h={64} fullHeight className={s.box1}>Block A</Box>
      <Box h={64} className={s.box2}>Block D</Box>
      <Box h={64} square className={s.box3} />
      <Box h={64} square className={s.box4} />
    </motion.div>
  )
}

export function ContentB() {
  return(
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ ease: "easeInOut", duration: 1 }}
      className={s.root}
      key="ContentB"
    >
      <Box h={64} fullHeight className={s.box1}>Block A</Box>
      <Box h={64} className={s.box2}>Block D</Box>
    </motion.div>
  )
}

export function ContentC() {
  return(
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ ease: "easeInOut", duration: 1 }}
      className={s.root}
      key="ContentC"
    >
      <Box h={64} fullHeight className={s.box1}>Block A</Box>
      <Box h={64} square className={s.box3} />
      <Box h={64} square className={s.box4} />
    </motion.div>
  )
}