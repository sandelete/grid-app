import { motion } from "framer-motion"
import s from './Root.module.css'

export function Root() {
  return (
    <motion.div
      initial={{ opacity: 0, y: -16 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ ease: "easeInOut", duration: 0.6 }}
      className={s.root}
    >
      Hello!
    </motion.div>
  )
} 
