import s from './Box.module.css'
import clsx from 'clsx'

export function Box({ h = 64, w = 1, fullHeight = false, square = false, className, children }) {
  return(
    <div className={clsx(s.root, className)}>
      <div
        className={s.paper}
        style={{ 
          minHeight: h, 
          height: fullHeight && '100%',
          aspectRatio: square && '1 / 1',
        }}>
        {children}
      </div>
    </div>
  )
}