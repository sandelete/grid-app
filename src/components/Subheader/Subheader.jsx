import clsx from 'clsx'
import React from 'react'
import s from './Subheader.module.css'

const TABS = ['First tab', 'Second tab', 'Third tab']

export const Subheader = ({ activeTabIndex, setActiveTabIndex }) => {
  return(
    <div className={s.root}>
      <div className={s.tabs}>
        {TABS.map((tab, index) => {
          const isActive = index === activeTabIndex

          return (
            <React.Fragment key={tab}>
              {index !== 0 && <span className={s.divider}/>}
              <span
                className={clsx(s.tab, { [s.active]: isActive })}
                onClick={() => setActiveTabIndex(index)}
              >{tab}</span>
            </React.Fragment>
          )
        })}
      </div>
    </div>
  )
}
