import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import React from 'react'
import ReactDOM from 'react-dom/client'

import { Layout } from './components'
import { Error, Game, Root, Tabs } from './pages'
import './styles'

const ROUTES = [
  { path: '/', element: <Root />, title: 'Home', icon: 'sparkles' },
  { path: '/tabs', element: <Tabs />, title: 'Tabs', icon: 'table' },
  { path: '/game', element: <Game />, title: 'Game', icon: '' },
  { path: '', title: 'Drag&Drop', icon: '' },
  { path: '', title: 'Gant', icon: '' },
  { path: '', title: 'Todos', icon: '' },
  { path: '', title: 'Login', icon: '' },
  { path: '', title: 'Firebase', icon: '' },
  { path: '', title: 'Tables', icon: '' },
]

const children = ROUTES.map(({ path, element }) => ({ path, element }))

const router = createBrowserRouter([
  {
    element: <Layout routes={ROUTES} />,
    errorElement: <Error />,
    children,
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>,
)
