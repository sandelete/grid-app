import { useState } from 'react'
import { ContentA, ContentB, ContentC, Subheader } from '/src/components'
import { motion, AnimatePresence } from 'framer-motion'
import s from './Tabs.module.css'

export function Tabs() {
  const [activeTabIndex, setActiveTabIndex] = useState(0)

  return (
    <motion.div
      initial={{ opacity: 0, translateY: -8 }}
      animate={{ opacity: 1, translateY: 0 }}
      // exit={{ opacity: 0, translateY: -100 }}
      transition={{ ease: 'easeOut', duration: 0.6 }}
      className={s.root}
    >
      <Subheader activeTabIndex={activeTabIndex} setActiveTabIndex={setActiveTabIndex} />
      {activeTabIndex === 0 && <ContentA />}
      {activeTabIndex === 1 && <ContentB />}
      {activeTabIndex === 2 && <ContentC />}
    </motion.div>
  )
}
